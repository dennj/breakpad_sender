//	Dennj Osele
//	12 Aug 2018
// clang-format -i -style=Google *.h *.cpp

#include "stdafx.h"

static bool dumpCallback(const google_breakpad::MinidumpDescriptor &descriptor, void *context, bool succeeded) {
  (void)context;
  if (succeeded) {
    std::map<string, string> parameters;
    std::map<string, string> files;
    std::string proxy_host;
    std::string proxy_userpasswd;

    // See above for the URL section for how to generate the proper url for your
    // server
    std::string url("http://yourcompany.sp.backtrace.io:6097/"
                    "post?format=minidump&token=57f2126dcef18bb0d2af35ec1d813f3775ee8228d1d886de522b2aedceff8b87");

    // Add any attributes to the parameters map.
    // Attributes such as uname.sysname, uname.version, cpu.count are
    // extracted from minidump files automatically.
    parameters["product_name"] = "breakpad_sender";
    parameters["version"] = "0.1.0";
    // parameters["uptime"] uptime_sec;

    files["upload_file_minidump"] = descriptor.path();

    std::string response, error;
    bool success = google_breakpad::HTTPUpload::SendRequest(url, parameters, files, proxy_host, proxy_userpasswd, "", &response, NULL, &error);
  }
  return succeeded;
}

void crash() {
  volatile int *a = (int *)(NULL);
  *a = 1;
}

int main() {
  google_breakpad::MinidumpDescriptor descriptor("/tmp");
  google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback, NULL, true, -1);

  crash();

  return 0;
}
